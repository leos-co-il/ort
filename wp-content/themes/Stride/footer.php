<?php

$tel = opt('tel');
$mail = opt('mail');
$address_all = opt('filials');
$fax = opt('fax');

$locations = get_nav_menu_locations();
$menu_obj = wp_get_nav_menu_object($locations['footer-links-menu']);
$links_menu_title = get_field('menu_title', $menu_obj);
$menu_obj_main = wp_get_nav_menu_object($locations['footer-menu']);
$foo_menu_title = get_field('menu_title', $menu_obj_main);
$menu_obj_services = wp_get_nav_menu_object($locations['footer-links-menu-2']);
$foo_services_title = get_field('menu_title', $menu_obj_services);
?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<a id="go-top">
					<img src="<?= ICONS ?>go-top.png" alt="to-top">
					<span class="top-text">
						חזרה
						למעלה
					</span>
				</a>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<div class="container">
			<?php if ($logo = opt('logo')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="/" class="logo logo-foo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-sm-11 col-12 footer-form">
					<div class="row justify-content-center align-items-end">
						<?php if ($f_title = opt('foo_form_title')) : ?>
							<div class="col-auto">
								<h2 class="foo-form-title"><?= $f_title; ?></h2>
							</div>
						<?php endif;
						if ($f_subtitle = opt('foo_form_subtitle')) : ?>
							<div class="col-auto">
								<h3 class="foo-form-subtitle"><?= $f_subtitle; ?></h3>
							</div>
						<?php endif; ?>
					</div>
					<?php getForm('291'); ?>
				</div>
			</div>
		</div>
		<div class="container footer-container-menu">
			<div class="row justify-content-sm-between justify-content-center align-items-stretch foo-after-title">
				<div class="col-lg col-sm-6 col-12 foo-menu">
					<h3 class="foo-title">
						<?= $foo_menu_title ? $foo_menu_title : esc_html__('ניווט מהיר', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-12 foo-menu foo-links-menu-2">
					<h3 class="foo-title">
						<?= $foo_services_title ? $foo_services_title : esc_html__('השירותים שלנו', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-services-menu', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-12 foo-menu foo-links-menu-2">
					<div>
						<h3 class="foo-title">
							<?= $links_menu_title ? $links_menu_title : esc_html__('חם מהבלוג', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
						</div>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-12 foo-menu foo-contact-menu">
					<h3 class="foo-title">
						פרטי התקשרות
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($address_all) : ?>
								<li>
									<h3 class="contact-foo-title">
										<span>
											<img src="<?= ICONS ?>foo-geo.png" alt="geo">
										</span>
										כתובות:
									</h3>
									<ul>
										<?php foreach ($address_all as $address) : if (isset($address['addres']) && $address['addres']) : ?>
											<li>
												<a href="https://waze.com/ul?q=<?= $address['addres']; ?>" class="contact-info-footer" target="_blank">
													<?= $address['addres']; ?>
												</a>
											</li>
										<?php endif; endforeach; ?>
									</ul>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<span class="contact-foo-title">
											<img src="<?= ICONS ?>foo-mail.png" alt="email">
											מייל:
										</span>
										<?= $mail; ?>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<span>
											<img src="<?= ICONS ?>foo-tel.png" alt="phone">
										</span>
										<?= $tel; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
