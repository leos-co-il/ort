<?php

the_post();
get_header();
$fields = get_fields();
$query = get_queried_object();
$products = get_posts([
    'numberposts' => 4,
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
$terms = get_terms([
    'taxonomy'      => 'product_cat',
    'hide_empty'    => false,
    'parent'        => 0
]);
$cat_img = get_field('cat_img', $query);
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-end">
			<div class="<?= $terms ? 'col-lg-9 col-12' : 'col-12'; ?>">
				<h1 class="base-title text-right mb-2"><?= $query->name; ?></h1>
				<h2 class="product-cat-subtitle">
					הכירו את השירותים והמוצרים שלנו
				</h2>
			</div>
		</div>
		<div class="row border-bottom-body">
			<?php if ($terms) : ?>
				<div class="col-lg-3">
					<div class="term-column">
						<?php foreach ($terms as $term) : ?>
							<a href="<?= get_term_link($term); ?>" class="term-side-link
							<?= ($term->term_id == $query->term_id) ? 'active' : ''; ?>">
								<?= $term->name; ?>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="<?= $terms ? 'col-lg-9 col-12' : 'col-12'; ?>">
				<div class="floated-container">
					<div class="floated-col">
						<?php if ($gal = get_field('product_gallery', $query)) : ?>
							<div class="product-cat-gallery arrows-slider arrows-slider-base mb-5">
								<div class="base-slider" dir="rtl">
									<?php if ($cat_img) : ?>
										<div>
											<a class="gal-item-product" href="<?= $cat_img['url']; ?>" data-lightbox="gallery"
											   style="background-image: url('<?= $cat_img['url']; ?>')"></a>
										</div>
									<?php endif;
									foreach ($gal as $img) : ?>
										<div>
											<a class="gal-item-product" href="<?= $img['url']; ?>" data-lightbox="gallery"
												 style="background-image: url('<?= $img['url']; ?>')"></a>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php elseif ($cat_img) : ?>
							<div class="main-post-image">
								<img src="<?= $cat_img['url']; ?>" alt="post-image">
							</div>
						<?php endif;
						get_template_part('views/partials/repeat', 'form_item'); ?>
					</div>
					<div class="base-output post-output">
						<?= category_description(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$field_posts = get_field('same_posts', $query);
if ($field_posts) {
	$samePosts = $field_posts;
} elseif ($basicPosts = opt('same_posts')) {
	$samePosts = $basicPosts;
} else {
	$samePosts = $products;
}
$services_link = $fields['same_link'] ? $fields['same_link'] : opt('all_serv_link');
$sameTitle = get_field('same_title', $query);
if ($samePosts) {
	get_template_part('views/partials/output', 'posts', [
		'posts' => $samePosts,
		'title' => $sameTitle ? $sameTitle : 'שירותים נוספים שיעניינו אתכם:',
		'link' => $services_link,
		'link_title' => 'עברו לדף שירותים'
	]);
}
if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => get_field('faq_title', $query),
			'faq' => $faq,
		]);
endif;
get_footer(); ?>
