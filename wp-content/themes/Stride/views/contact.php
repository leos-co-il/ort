<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$fax = opt('fax');
$mail = opt('mail');
$address_all = opt('filials');
?>
<article class="page-body contact-page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-12">
				<div class="row justify-content-center">
					<?php if ($logo = opt('logo')) : ?>
						<div class="col-auto">
							<a href="/" class="logo logo-contact">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					<?php endif; ?>
					<div class="col-12">
						<h1 class="base-title contact-base-title mb-2"><?php the_title(); ?></h1>
						<?php if ($fields['contact_page_subtitle']) : ?>
							<h2 class="contact-form-subtitle"><?= $fields['contact_page_subtitle']; ?></h2>
						<?php endif; ?>
						<div class="base-output text-center">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<div class="row align-items-stretch">
					<div class="col-12 d-flex justify-content-center">
						<div class="contact-page-form mb-60">
							<?php getForm('24'); ?>
							<div class="contact-page-line">
								<div class="row justify-content-lg-center justify-content-start align-items-start">
									<?php if ($tel) : ?>
										<div class="col-lg col-sm-auto col-12 contact-item">
											<a href="tel:<?= $tel; ?>" class="contact-item-link">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>foo-tel.png">
												<span class="contact-title">
													טלפון ראשי:
												</span>
											</span>
												<span class="contact-info">
												<span class="contact-type"><?= ' '.$tel; ?></span>
												</span>
											</a>
										</div>
									<?php endif; ?>
									<?php if ($fax) : ?>
										<div class="col-lg col-sm-auto col-12 contact-item">
											<div class="contact-item-link">
												<div class="contact-icon-wrap">
													<img src="<?= ICONS ?>foo-fax.png">
													<span class="contact-title">
														פקס:
													</span>
												</div>
												<div class="contact-info">
													<span class="contact-type"><?= ' '.$fax; ?></span>
												</div>
											</div>
										</div>
									<?php endif;
									if ($mail) : ?>
										<div class="col-lg col-sm-auto col-12 contact-item">
											<a href="mailto:<?= $mail; ?>" class="contact-item-link">
												<div class="contact-icon-wrap">
													<img src="<?= ICONS ?>foo-mail.png">
													<span class="contact-title">
														מייל:
													</span>
												</div>
												<div class="contact-info">
													<span class="contact-type"><?= ' '.$mail; ?></span>
												</div>
											</a>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($address_all) : ?>
		<div class="container-fluid mb-60">
			<div class="row">
				<?php foreach ($address_all as $addr) : if ($addr['iframe']) : ?>
					<div class="col-md-4 col-12 filial-col">
						<div class="contact-filial">
							<div class="mb-2 flex-grow-1">
								<span class="contact-item-title"><?= $addr['fil_title']; ?></span>
								<a href="https://waze.com/ul?q=<?= $addr['addres']; ?>" class="base-text">
									<?= $addr['addres']; ?>
								</a>
							</div>
							<div class="iframe-wrapper-contact">
								<?= $addr['iframe']; ?>
							</div>
						</div>
					</div>
				<?php endif; endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
