<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main" <?php if ($fields['home_main_img']) : ?>
	style="background-image: url('<?= $fields['home_main_img']['url']; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row align-items-center">
			<div class="home-col col-xl-5 col-12 d-flex flex-column align-items-start">
				<div class="home-main-text">
					<?= $fields['home_main_text']; ?>
				</div>
				<?php if ($fields['home_main_link']) : ?>
					<a href="<?= $fields['home_main_link']['url']; ?>" class="base-link home-link">
						<?= (isset($fields['home_main_link']['title']) && $fields['home_main_link']['title']) ?
								$fields['home_main_link']['title'] : 'לחצו ליצירת קשר'; ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($fields['home_main_img']) : ?>
			<div class="row home-img-row">
				<div class="col-12">
					<img src="<?= $fields['home_main_img']['url']; ?>" alt="<?=$fields['home_main_img']['title']; ?>">
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ($fields['h_cats']) : ?>
	<section class="home-services-block">
		<div class="container-fluid">
			<div class="row">
				<?php if ($fields['h_cats_title']) : ?>
					<div class="col-12">
						<h2 class="services-block-title">
							<?= $fields['h_cats_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
				<div class="col-12">
					<div class="video-slider-wrap">
						<div class="services-slider" dir="rtl">
							<?php foreach ($fields['h_cats'] as $cat) : ?>
								<div class="p-1">
									<a class="service-card" href="<?= get_term_link($cat); ?>">
										<span class="serv-item-title"><?= $cat->name; ?></span>
										<?php if ($img = get_field('cat_img', $cat)) : ?>
											<span class="serv-img-wrap">
													<img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>">
												</span>
										<?php endif; ?>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_about_text'] || $fields['h_video']) : ?>
	<section class="home-about-block">
		<div class="container mb-5">
			<div class="row">
				<div class="col-12">
					<h1 class="base-title text-right mb-2"><?= $fields['h_about_title']; ?></h1>
				</div>
			</div>
			<div class="row align-items-center justify-content-between">
				<?php if ($fields['h_about_text']) : ?>
					<div class="<?= $fields['h_video'] ? 'col-lg-6 col-12' : 'col' ?> d-flex flex-column align-items-start">
						<div class="base-output post-output about-output">
							<?= $fields['h_about_text']; ?>
						</div>
						<?php if ($fields['h_about_link']) : ?>
							<a href="<?= $fields['h_about_link']['url'];?>" class="base-link">
								<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title']) ?
										$fields['h_about_link']['title'] : 'המשיכו לקרוא'; ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($fields['h_video']) : ?>
					<div class="<?= $fields['h_about_text'] ? 'col-lg-6 col-12' : 'col' ?>">
						<div class="video-about">
							<?= $fields['h_video']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<div class="form-reviews-home">
	<?php if ($fields['h_team_item']) : ?>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="team-block-wrap">
						<img src="<?= ICONS ?>pen.png" alt="pen" class="pen-abs">
						<?php if ($fields['h_team_title']) : ?>
							<h2 class="base-title-white font-weight-bold mb-5">
								<?= $fields['h_team_title']; ?>
							</h2>
						<?php endif; ?>
						<div class="row">
							<?php foreach ($fields['h_team_item'] as $item) : ?>
								<div class="col-lg-6 col-12 team-home-col">
									<div class="home-team-item">
										<div class="home-team-img">
											<?php if ($item['img']) : ?>
												<img src="<?= $item['img']['url']; ?>" alt="<?= $item['img']['alt']; ?>">
											<?php endif; ?>
										</div>
										<h3 class="home-team-title">
											<?= $item['name']; ?>
										</h3>
										<div class="base-output text-center">
											<?= $item['text']; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php if ($fields['h_team_link']) : ?>
							<div class="row justify-content-center mt-3">
								<div class="col-auto">
									<a href="<?= $fields['h_team_link']['url'];?>" class="base-link">
										<?= (isset($fields['h_team_link']['title']) && $fields['h_team_link']['title']) ?
												$fields['h_team_link']['title'] : 'המשיכו לקרוא'; ?>
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['h_blog']) : ?>
	<section class="same-posts">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="base-title">
						<?= $fields['h_blog_title'] ? $fields['h_blog_title'] : ''; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch row-minus-margin">
				<?php foreach ($fields['h_blog'] as $i => $post) : ?>
					<div class="col-lg-4 post-col-home">
						<?php get_template_part('views/partials/card', 'post_home', [
								'post' => $post,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($fields['h_blog_link']) : ?>
				<div class="row justify-content-center mt-5">
					<div class="col-auto">
						<a href="<?= $fields['h_blog_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_blog_link']['title']) && $fields['h_blog_link']['title']) ? $fields['h_blog_link']['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['h_video_slider']) {
	get_template_part('views/partials/output', 'video_slider', [
			'video' => $fields['h_video_slider'],
			'title' => $fields['h_video_sl_title'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
