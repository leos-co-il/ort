<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms([
	'post_type' => 'product',
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="base-title page-title mb-2">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($cats) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($cats as $cat) : ?>
					<div class="col-xl-4 col-md-6 col-12 post-blog-col post-products-col">
						<?php get_template_part('views/partials/card', 'category',
								[
										'post' => $cat,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>

