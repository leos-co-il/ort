<div class="repeat-form-post">
	<?php if ($title = opt('post_form_title')) : ?>
		<h2 class="post-form-title"><?= $title; ?></h2>
	<?php endif;
	if ($subtitle = opt('post_form_subtitle')) : ?>
		<h3 class="post-form-subtitle"><?= $subtitle; ?></h3>
	<?php endif; ?>
	<div class="base-form-wrap">
		<?php getForm('38'); ?>
	</div>
</div>
