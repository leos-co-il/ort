<?php if (isset($args['video']) && $args['video']) : ?>
	<div class="home-video-slider">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="video-slider-wrap">
						<div class="row">
							<div class="col-12">
								<h2 class="video-block-title">
									<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'סרטונים'; ?>
								</h2>
							</div>
						</div>
						<div class="video-slider" dir="rtl">
							<?php foreach ($args['video'] as $video) : if ($video['link']) : ?>
								<div class="p-1">
									<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video['link']); ?>')">
										<div class="video-item-overlay play-button"
											 data-video="<?= getYoutubeId($video['link']); ?>">
											<img src="<?= ICONS ?>play.png" alt="play-video">
										</div>
									</div>
									<?php if ($video['title']) : ?>
										<h4 class="video-item-title">
											<?= $video['title']; ?>
										</h4>
									<?php endif; ?>
								</div>
							<?php endif; endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="video-modal">
		<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
			 aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body" id="iframe-wrapper"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="close-icon">×</span>
					</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
