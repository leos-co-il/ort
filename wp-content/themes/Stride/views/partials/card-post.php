<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
		<a class="post-item-image" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
		</a>
		<div class="post-item-content">
			<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="base-text">
				<?= text_preview($args['post']->post_content, 20); ?>
			</p>
		</div>
		<a class="post-item-link" href="<?= $link; ?>">
			<span>
				המשיכו לקרוא
			</span>
			<img src="<?= ICONS ?>link-blue.png" alt="read-more">
		</a>
	</div>
<?php endif; ?>
