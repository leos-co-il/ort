<?php if (isset($args['posts']) && $args['posts']) : ?>
	<div class="same-posts">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="base-title">
						<?= isset($args['title']) && $args['title'] ? $args['title'] : ''; ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['posts'] as $i => $post) : ?>
					<div class="col-xl-3 col-md-6 col-12 post-col-same">
						<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($args['link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $args['link']['url'];?>" class="base-link">
							<?= (isset($args['link']['title']) && $args['link']['title']) ? $args['link']['title'] : $args['link_title']; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
