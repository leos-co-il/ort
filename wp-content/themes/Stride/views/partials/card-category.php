<?php if (isset($args['post']) && $args['post']) : $link = get_term_link($args['post']); ?>
	<div class="post-item more-card">
		<a class="post-item-image" href="<?= $link; ?>"
			<?php if ($img = get_field('cat_img', $args['post'])) : ?>
				style="background-image: url('<?= $img['url']; ?>')"
			<?php endif;?>>
		</a>
		<div class="post-item-content">
			<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->name; ?></a>
			<p class="base-text">
				<?= text_preview(category_description($args['post']), 20); ?>
			</p>
		</div>
		<a class="post-item-link" href="<?= $link; ?>">
			<span>
				המשיכו לקרוא
			</span>
			<img src="<?= ICONS ?>link-blue.png" alt="read-more">
		</a>
	</div>
<?php endif; ?>
