<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-item more-card">
		<div class="post-item-image" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
			<div class="post-overlay">
				<a class="post-item-link" href="<?= $link; ?>">
					<span>
						המשיכו לקרוא
					</span>
					<img src="<?= ICONS ?>link-left.png" alt="read-more">
				</a>
			</div>
		</div>
		<div class="post-item-content">
			<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="base-text">
				<?= text_preview($args['post']->post_content, 20); ?>
			</p>
		</div>
	</div>
<?php endif; ?>
