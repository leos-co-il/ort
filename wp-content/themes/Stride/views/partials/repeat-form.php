<div class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($logo = opt('logo_white')) : ?>
				<div class="col-auto">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
			<div class="col-12 col-repeat-form">
				<?php if ($f_title = opt('base_form_title')) : ?>
					<h2 class="base-form-title"><?= $f_title; ?></h2>
				<?php endif;
				getForm('37'); ?>
			</div>
		</div>
	</div>
</div>
