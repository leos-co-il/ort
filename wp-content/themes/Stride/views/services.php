<?php
/*
Template Name: שירותים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'product',
	'suppress_filters' => false
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="base-title page-title mb-2">
					<?php the_title(); ?>
				</h1>
				<div class="block-output text-center mb-5">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) : ?>
					<div class="col-xl-4 col-md-6 col-12 post-blog-col post-products-col">
						<?php get_template_part('views/partials/card', 'post',
							[
								'post' => $post,
							]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'text' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>

