<?php
/*
Template Name: אודות
*/

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container mb-5">
		<div class="row">
			<div class="col-12">
				<h1 class="base-title text-right mb-2"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="floated-container">
					<div class="floated-col">
						<?php if ($fields['about_video']) : ?>
							<div class="main-post-image video-about">
								<?= $fields['about_video']; ?>
							</div>
						<?php elseif (has_post_thumbnail()) : ?>
							<div class="main-post-image">
								<img src="<?= postThumb(); ?>" alt="post-image">
							</div>
						<?php endif;
						get_template_part('views/partials/repeat', 'form_item'); ?>
					</div>
					<div class="base-output post-output about-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
if ($fields['team_member']) : ?>
	<section class="team-block-about">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<?php if ($fields['team_title']) : ?>
						<h2 class="team-block-title"><?= $fields['team_title']; ?></h2>
					<?php endif; ?>
				</div>
				<?php foreach ($fields['team_member'] as $member) : ?>
					<div class="col-xl-10 col-md-11 col-12 member-col">
						<div class="member-card row justify-content-between align-items-center">
							<div class="<?= $member['img'] ? 'col-xl-7 col-lg-6 col-12' : 'col-12'?>; ">
								<?php if ($member['name']) : ?>
									<h3 class="member-name"><?= $member['name']; ?></h3>
								<?php endif; ?>
								<div class="base-output">
									<?= $member['text']; ?>
								</div>
							</div>
							<?php if ($member['img']) : ?>
								<div class="col-xl-4 col-lg-6 col-12">
									<img src="<?= $member['img']['url']; ?>" class="w-100" alt="<?= $member['img']['alt'];?>">
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>

