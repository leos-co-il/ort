<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block page-body pb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title my-3">
			<?= 'תוצאות חיפוש עבור: '.get_query_var('s') ?>
		</h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-xl-4 col-md-6 col-12 post-blog-col post-products-col">
					<div class="post-item more-card">
						<a class="post-item-image" href="<?= $link; ?>"
							<?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')"
							<?php endif;?>>
						</a>
						<div class="post-item-content">
							<a class="post-item-title" href="<?= $link; ?>"><?php the_title(); ?></a>
							<p class="base-text">
								<?= text_preview(get_the_content(), 20); ?>
							</p>
						</div>
						<a class="post-item-link" href="<?= $link; ?>">
			<span>
				המשיכו לקרוא
			</span>
							<img src="<?= ICONS ?>link-blue.png" alt="read-more">
						</a>
					</div>
				</div>
			<?php }
			} ?>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php if ( $the_query_2->have_posts() ) {
				while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
					<div class="col-xl-4 col-md-6 col-12 post-blog-col">
						<?php $link = get_the_permalink(); ?>
						<div class="post-item more-card">
							<a class="post-item-image" href="<?= $link; ?>"
								<?php if (has_post_thumbnail()) : ?>
									style="background-image: url('<?= postThumb(); ?>')"
								<?php endif;?>>
							</a>
							<div class="post-item-content">
								<a class="post-item-title" href="<?= $link; ?>"><?php the_title(); ?></a>
								<p class="base-text">
									<?= text_preview(get_the_content(), 20); ?>
								</p>
							</div>
							<a class="post-item-link" href="<?= $link; ?>">
			<span>
				המשיכו לקרוא
			</span>
								<img src="<?= ICONS ?>link-blue.png" alt="read-more">
							</a>
						</div>

					</div>
				<?php }
			} ?>
		</div>
		<?php if (!$the_query_1->have_posts() && !$the_query_2->have_posts()) : ?>
			<div class="col-12 pt-5">
				<h4 class="base-title">
					שום דבר לא נמצא
				</h4>
			</div>
			<div class="alert alert-info text-center mt-5">
				<p>
					מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.
				</p>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
