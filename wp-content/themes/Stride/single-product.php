<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="base-title text-right mb-2"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="floated-container">
					<div class="floated-col">
						<?php get_template_part('views/partials/repeat', 'form_item');?>
					</div>
					<div class="base-output post-output product-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php if (isset($fields['product_gallery']) && $fields['product_gallery']) : ?>
				<div class="col-12 arrows-slider arrows-slider-product">
					<div class="product-gallery-block">
						<h2 class="product-gallery-title">גלריית תמונות:</h2>
						<div class="services-slider" dir="rtl">
							<?php foreach ($fields['product_gallery'] as $image) : ?>
								<div class="p-2">
									<div class="gallery-item" style="background-image: url('<?= $image['url']; ?>')">
										<a class="gallery-overlay" href="<?= $image['url']; ?>"
										   data-lightbox="gallery"></a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if (isset($fields['product_text']) && $fields['product_text']) : ?>
				<div class="col-12">
					<div class="base-output post-output product-output">
						<?= $fields['product_text']; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => 'product',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'product',
			'post__not_in' => array($postId),
	]);
}
$blog_link = $fields['same_link'] ? $fields['same_link'] : opt('all_serv_link');
if ($phone = opt('prod_phone')) : ?>
	<section class="call-to-action-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-7 col-md-9 col-12 d-flex flex-column align-items-center">
					<?php if ($phone['title']) : ?>
						<h3 class="call-to-action-title"><?= $phone['title']; ?></h3>
					<?php endif;
					if ($phone['tel']) : ?>
						<a class="call-to-link" href="tel:<?= $phone['tel']; ?>">
							<?= $phone['tel']; ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
$sl_video = opt('video_slider');
if ($fields['video_slider'] || $sl_video) : ?>
<div class="video-slider-product">
	<?php get_template_part('views/partials/output', 'video_slider', [
			'video' => $fields['video_slider'] ? $fields['video_slider'] : $sl_video,
			'title' => $fields['slider_video_title'] ? $fields['slider_video_title'] : opt('slider_video_title'),
	]); ?>
</div>
<?php endif;
if ($samePosts) : ?>
	<div class="same-products">
		<?php 	get_template_part('views/partials/output', 'posts', [
				'posts' => $samePosts,
				'title' => $fields['same_title'] ? $fields['same_title'] : 'שירותים נוספים שיעניינו אתכם:',
				'link' => $blog_link,
				'link_title' => 'עברו לדף שירותים'
		]); ?>
	</div>
<?php endif;
if (isset($fields['faq_item']) && $fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
