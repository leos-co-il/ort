<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-between">
				<div class="col">
					<nav id="MainNav" class="h-100">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '1', '', 'main_menu h-100'); ?>
					</nav>
				</div>
				<div class="col-auto">
					<div class="row justify-content-end">
						<div class="col header-socials-line">
							<?php if ($facebook = opt('facebook')) : ?>
								<a href="<?= $facebook; ?>" class="header-social-link">
									<img src="<?= ICONS ?>facebook.png" alt="facebook">
								</a>
							<?php endif;
							if ($youtube = opt('youtube')) : ?>
								<a href="<?= $youtube; ?>" class="header-social-link">
									<img src="<?= ICONS ?>youtube.png" alt="youtube">
								</a>
							<?php endif;
							if ($tel = opt('tel')) : ?>
								<a href="tel:<?= $tel; ?>" class="header-tel-link">
								<span class="header-tel-icon">
									<img src="<?= ICONS ?>header-tel.png" alt="phone">
								</span>
									<span class="tel-number">
									<?= $tel; ?>
								</span>
								</a>
							<?php endif; ?>
						</div>
						<?php if ($logo = opt('logo')) : ?>
						<div class="col-auto">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<?php getMenu('header-bottom-menu', '1', '', 'main_menu h-100'); ?>
				</div>
			</div>
		</div>
	</div>
</header>


<div class="fixed-links">
	<div class="fixed-link search-trigger">
		<img src="<?= ICONS ?>search.png" alt="open-search">
	</div>
	<div class="fixed-link pop-trigger">
		<img src="<?= ICONS ?>chat.png" alt="open-popup">
	</div>
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a class="fixed-link whatsapp-link" href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>">
			<img src="<?= ICONS ?>whatsapp.png" alt="to-whatsapp">
		</a>
	<?php endif; ?>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxxl-9 col-lg-10 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop form-wrapper">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png" alt="close">
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="post-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('pop_form_subtitle')) : ?>
							<h3 class="post-form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('325'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pop-form-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 col-12 d-flex justify-content-center">
				<div class="float-form-search d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form-search">
							<img src="<?= ICONS ?>close.png" alt="close">
						</span>
						<?php if ($title_search = opt('search_title')) : ?>
							<h2 class="form-subtitle text-center mb-3"><?= $title_search; ?></h2>
						<?php endif;
						get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
