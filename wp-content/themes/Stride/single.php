<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="base-title text-right mb-2"><?php the_title(); ?></h1>
				<div class="d-flex justify-content-start align-items-center flex-wrap mb-4">
					<h2 class="post-time-title"><?= get_the_author().' | '; ?></h2>
					<h2 class="post-time-title"><?= get_the_date(); ?></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="floated-container">
					<div class="floated-col">
						<?php if (has_post_thumbnail()) : ?>
							<div class="main-post-image">
								<img src="<?= postThumb(); ?>" alt="post-image">
							</div>
						<?php endif;
						get_template_part('views/partials/repeat', 'form_item'); ?>
					</div>
					<div class="base-output post-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
$blog_link = $fields['same_link'] ? $fields['same_link'] : opt('all_post_link');
if ($samePosts) {
	get_template_part('views/partials/output', 'posts', [
		'posts' => $samePosts,
		'title' => $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים שיעניינו אתכם:',
		'link' => $blog_link,
		'link_title' => 'עברו לדף מאמרים'
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
